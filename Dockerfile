# FROM node:10.15.3

# WORKDIR /app

# ENV PATH /app/node_modules/.bin:$PATH

# COPY package.json /app/package.json
# RUN npm install
# RUN npm install -g @angular/cli@7.3.9

# COPY . /app

# CMD ng serve --host 0.0.0.0

FROM node:12-alpine AS builder
COPY . ./winnable/
WORKDIR /winnable/
RUN npm install
RUN $(npm bin)/ng build --configuration="production"

FROM nginx:1.15.8-alpine
COPY --from=builder /dist/winnable/ /usr/share/nginx/html