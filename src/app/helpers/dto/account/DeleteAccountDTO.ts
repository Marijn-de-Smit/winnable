export class DeleteAccountDTO {
  
  constructor(
    public accountId: number,
    public adminId: number,
  ) { }
}
