export class AddAccountDTO {

  constructor(
    public emailAddress: string = "",
    public password: string = "",
    public passwordConfirm: string = "",
    public firstname: string = "",
    public infix: string = "",
    public lastname: string = "",
    public phoneNumber: string = "",
  ) { }
}
