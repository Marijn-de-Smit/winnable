export class RegistrationRequestDTO {
    constructor(
        public firstname: string,
        public lastname: string = "",
        public emailAddress: string = "",
        public phone: string = "",
        public password: string = "",
        public passwordConfirm: string = "",
    ) { }
}
