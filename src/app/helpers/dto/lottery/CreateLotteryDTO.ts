import { Category } from '../../enums/Category';

export class CreateLotteryDTO {
  
  name: string;
  category: Category;

  constructor(name: string, category: Category) {
     this.name = name;
     this.category = category;
  }
}