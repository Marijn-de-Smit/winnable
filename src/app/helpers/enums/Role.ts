export enum Role {
  MANAGER = "MANAGER",
  PROVIDER = "PROVIDER",
  USER = "USER",
  GUEST = "GUEST",
}