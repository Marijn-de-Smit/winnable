import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { LotteryComponent } from './pages/lottery/lottery.component';
import { ProviderDashboardComponent } from './pages/provider-dashboard/provider-dashboard.component';
import { MaintainerDashboardComponent } from './pages/maintainer-dashboard/maintainer-dashboard.component';
import { LotteriesComponent } from './pages/lotteries/lotteries.component';
import {AuthGuardService} from 'src/app/service/authentication/authguard.service';
import { PolicyComponent } from './pages/policy/policy.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'home', component: HomeComponent, canActivate: [AuthGuardService]},
  {path: 'maintainer', component: MaintainerDashboardComponent, canActivate: [AuthGuardService]},
  {path: 'provider', component: ProviderDashboardComponent, canActivate: [AuthGuardService]},
  {path: 'profile', component: ProfileComponent, canActivate: [AuthGuardService]},
  {path: 'lotteries', component: LotteriesComponent, canActivate: [AuthGuardService]},
  {path: 'lottery', component: LotteryComponent, canActivate: [AuthGuardService]},
  {path: 'policy', component: PolicyComponent},
  {path: '**', redirectTo: '/404'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule {
}
