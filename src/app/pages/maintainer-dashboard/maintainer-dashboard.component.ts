import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-maintainer-dashboard',
  templateUrl: './maintainer-dashboard.component.html',
  styleUrls: ['./maintainer-dashboard.component.css']
})
export class MaintainerDashboardComponent implements OnInit {

  constructor(
    private router: Router,
  ) { }

  ngOnInit() {
  }

}
