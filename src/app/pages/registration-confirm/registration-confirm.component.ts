import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';

@Component({
  selector: 'app-registration-confirm',
  templateUrl: './registration-confirm.component.html',
  styleUrls: ['./registration-confirm.component.css']
})
export class RegistrationConfirmComponent implements OnInit {

  private token: string;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit() {
    this.retrieveTokenFromUrl();
  }

  retrieveTokenFromUrl(){
    this.route.params.subscribe(params => {         
        this.token = params["token"];   
        this.registrationConfirm();
   });
  }

  registrationConfirm() {
    console.log(this.token);
    this.authenticationService.registrationConfirm(this.token)
      .subscribe(
        data => {
          console.log(data);
          this.router.navigateByUrl("login");
        },
        error => {
          console.log(error);
        });
  }
}