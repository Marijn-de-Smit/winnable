import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LotteryService } from 'src/app/service/lottery/lottery.service';

@Component({
  selector: 'app-lottery',
  templateUrl: './lottery.component.html',
  styleUrls: ['./lottery.component.css']
})
export class LotteryComponent implements OnInit {

  constructor(
    private router: Router,
    private lotteryService: LotteryService
  ) { }

  model: any = {};
  lottery: any;

  ngOnInit() {
    this.retrieveLottery;
  }

  retrieveLottery(id) {
    this.lotteryService.getbyId(id)
      .subscribe(
        data => {
          this.lottery = data.entity;
        },
        error => {
          console.log(error);
        });
  }
}
