import {Component, OnInit, Pipe, PipeTransform} from '@angular/core';
import {Router} from '@angular/router';

import { CreateLotteryDTO } from 'src/app/helpers/dto/lottery/CreateLotteryDTO';
import { UpdateLotteryDTO } from 'src/app/helpers/dto/lottery/UpdateLotteryDTO';
import { DeleteLotteryDTO } from 'src/app/helpers/dto/lottery/DeleteLotteryDTO';

import { Category } from 'src/app/helpers/enums/Category';

import { LotteryService } from 'src/app/service/lottery/lottery.service';

@Component({
  selector: 'app-provider-dashboard',
  templateUrl: './provider-dashboard.component.html',
  styleUrls: ['./provider-dashboard.component.css']
})
export class ProviderDashboardComponent implements OnInit {

  data: any = {
    entity: {},
    lotteries: {},
  };

  Category = Category;

  model: any = {};
  type: String;
  Message = "";
  ErrorMessage = "";
  loading = false;

  selected: number;
  method: String;
  lotteries: Array<object>;

  constructor(
    private router: Router,
    private lotteryService: LotteryService
  ) { }

  ngOnInit() {
    this.selected = 0;
    this.method = "create";

    this.data.lotteries = this.readAll();
  }

  checkCreate(type) {
    switch (type) {
      case "lottery":
        return new CreateLotteryDTO(this.model.lotteryName, this.model.category.toString());
      default:
        return false;
    }
  }

  checkUpdate(type) {

    switch (type) {
      case "lottery":
        return new UpdateLotteryDTO(this.selected, this.model.lotteryName, this.model.category.toString());
      default:
        return false;
    }
  }

  create() {
    let dto = this.checkCreate(this.type);
    this.lotteryService.create(dto)
      .subscribe(
        data => {
          if (data != null) {
            this.Message = "Record added succesfully!";
          } else {
            this.ErrorMessage = "Something went wrong!";
          }
          this.loading = false;
        }
      );
  }

  readAll() {
    this.lotteryService.getAll()
      .subscribe(
        data => {
          this.data.lotteries = data.entity;
        },
        error => {
          this.ErrorMessage = "Something went wrong!";
        });
  }

  readById() {
    this.lotteryService.getbyId(this.selected)
      .subscribe(
        data => {
          this.data.entity = data;
        },
        error => {
          this.ErrorMessage = "Something went wrong!";
        });
  }

  update() {
    let dto = this.checkUpdate(this.type);
    this.lotteryService.update(dto)
      .subscribe(
        data => {
          if (data != null) {
            this.Message = "Record updated succesfully!";
          } else {
            this.ErrorMessage = "Something went wrong!";
          }
          this.loading = false;
        }
      );
  }

  delete() {
    this.lotteryService.delete(this.selected)
      .subscribe(
        data => {
          if (data != null) {
            this.Message = "Record deleted succesfully!";
          } else {
            this.ErrorMessage = "Something went wrong!";
          }
          this.loading = false;
        }
      );
  }

  changeState(id, method, type) {
    this.type = type;
    this.selected = id;
    this.method = method;
  }

  submit() {
    console.log(this.method)
    switch (this.method) {
      case "create":
        this.create();
        break;
      case "update":
        this.update();
        break;
      case "delete":
        this.delete();
        break;
      default:
        return false;
    }
  }
}

@Pipe({
  name: 'enumToArray'
})
export class EnumToArrayPipe implements PipeTransform {
  transform(data: Object) {
    return Object.keys(data);
  }
}
