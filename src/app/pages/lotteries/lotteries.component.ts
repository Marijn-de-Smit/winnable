import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { LotteryService } from 'src/app/service/lottery/lottery.service';

@Component({
  selector: 'app-lotteries',
  templateUrl: './lotteries.component.html',
  styleUrls: ['./lotteries.component.css']
})
export class LotteriesComponent implements OnInit {

  model: any = {};
  lotteries: any;

  constructor(
    private router: Router,
    private lotteryService: LotteryService
  ) { }

  ngOnInit() {
    this.retrieveLotteries;
  }

  retrieveLotteries() {
    this.lotteryService.getAll()
      .subscribe(
        data => {
          this.lotteries = data.entity;
        },
        error => {
          console.log(error);
        });
  }

}
