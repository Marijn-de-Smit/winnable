import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { WalletService } from 'src/app/service/wallet/wallet.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  model: any = {};
  wallet: any;

  constructor(
    private router: Router,
    private walletService: WalletService
  ) { }

  ngOnInit() {
    this.retrieveWallet;
  }

  retrieveWallet() {
    this.walletService.getbyId(1)
      .subscribe(
        data => {
          this.wallet = data.entity;
        },
        error => {
          console.log(error);
        });
  }

  deposit(account){
    this.walletService.update(account)
      .subscribe(
        data => {
          this.wallet = data;
        },
        error => {
          console.log(error);
        });
  }

  withdraw(account){
    this.walletService.update(account)
      .subscribe(
        data => {
          this.wallet = data;
        },
        error => {
          console.log(error);
        });
  }
}
