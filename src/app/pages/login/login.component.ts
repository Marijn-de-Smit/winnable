import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from 'src/app/service/authentication/authentication.service';
import { AuthenticationDTO } from 'src/app/helpers/dto/AuthenticationDTO';
import * as $ from 'jquery';
import { RegistrationRequestDTO } from 'src/app/helpers/dto/RegistrationDTO';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model: any = {};
  loading = false;
  returnUrl: string;
  userLoggedIn: string;
  loggedInId: number;
  resultMessage = "";
  errorMessage: string[] = [];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
  ) { }

  ngOnInit(): void {
    $('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
      var $this = $(this),
          label = $this.prev('label');
    
        if (e.type === 'keyup') {
          if ($this.val() === '') {
              label.removeClass('active highlight');
            } else {
              label.addClass('active highlight');
            }
        } else if (e.type === 'blur') {
          if( $this.val() === '' ) {
            label.removeClass('active highlight'); 
          } else {
            label.removeClass('highlight');   
          }   
        } else if (e.type === 'focus') {
          
          if( $this.val() === '' ) {
            label.removeClass('highlight'); 
          } 
          else if( $this.val() !== '' ) {
            label.addClass('highlight');
          }
        }
    
    });
    
    $('.tab a').on('click', function (e) {
      
      e.preventDefault();
      
      $(this).parent().addClass('active');
      $(this).parent().siblings().removeClass('active');
      
      var target = $(this).attr('href');
    
      $('.tab-content > div').not(target).hide();
      
      $(target).fadeIn(600);
      
    });
  }

  login() {
    let dto = new AuthenticationDTO(this.model.mail, this.model.password);
    this.authenticationService.login(dto)
    .subscribe(
      data => {
        if(data != null) {
          this.setStorageData(data);
        }else{
          this.loading = false;
          this.errorMessage.push("Something went wrong! Did you enter the correct credentials?");
        }
      },
      error => {
          console.log(error);
          this.loading = false;
          this.errorMessage.push("Incorrect e-mail or password entered");          
      });
  }

  register() {
    this.loading = true;
    let dto = new RegistrationRequestDTO(this.model.firstname, this.model.lastname, this.model.email, this.model.phone, this.model.password, this.model.passwordConfirm);
    console.log(JSON.stringify(dto));
    this.authenticationService.register(dto)
    .subscribe(
      data => {
        this.loading = false;
        this.resultMessage = data.message;
        this.router.navigateByUrl("login");
      },
      error => {
        console.log(error);
        this.errorMessage.push(error);
        this.loading = false;
        this.router.navigateByUrl("login");
      });
  }

  setStorageData(data: any){
    localStorage.setItem('token', data.entity.token);
    localStorage.setItem("accountRole", data.entity.role);
    this.router.navigateByUrl('/home'); 
  }
}
