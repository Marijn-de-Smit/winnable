import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppRoutingModule } from './app-routing';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { MaintainerDashboardComponent } from './pages/maintainer-dashboard/maintainer-dashboard.component';
import { ProviderDashboardComponent, EnumToArrayPipe } from './pages/provider-dashboard/provider-dashboard.component';
import { LotteryComponent } from './pages/lottery/lottery.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { PanelComponent } from './components/panel/panel.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LotteriesComponent } from './pages//lotteries/lotteries.component';
import { ErrorComponent } from './pages/error/error.component';
import { PolicyComponent } from './pages/policy/policy.component';
import { AuthenticationService } from './service/authentication/authentication.service';
import { LotteryService } from './service/lottery/lottery.service';
import { WalletService } from './service/wallet/wallet.service';
import { ProviderService } from './service/provider/provider.service';
import { ManagerService } from './service/manager/manager.service';
import { Restdata } from './helpers/restdata';
import { AuthGuardService } from './service/authentication/authguard.service';
import { AccountService } from './service/authentication/account.service';
import { RegistrationConfirmComponent } from './pages/registration-confirm/registration-confirm.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    LoginComponent,
    HomeComponent,
    MaintainerDashboardComponent,
    ProviderDashboardComponent,
    EnumToArrayPipe,
    LotteryComponent,
    ProfileComponent,
    PanelComponent,
    LotteriesComponent,
    ErrorComponent,
    PolicyComponent,
    RegistrationConfirmComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [
    AccountService,
    AuthenticationService,
    WalletService,
    ManagerService,
    ProviderService,
    LotteryService,
    Restdata,
    AuthGuardService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
