import { environment } from '../environments/environment';

export let CONFIG = {

  baseUrls: {
    loginRequest: 'http://localhost:8082/user/login/',
    registerRequest: 'http://localhost:8082/user/register/',
    createLottery: 'http://localhost:8083/provider/lottery/create',
    readLottery: 'http://localhost:8083/provider/lottery/get',
    readLotteries: 'http://localhost:8083/provider/lottery/getall',
    updateLottery: 'http://localhost:8083/provider/lottery/update',
    deleteLottery: 'http://localhost:8083/provider/lottery/delete',
    readWallet: 'http://localhost:8082/payment/get/',
    depositRequest: 'http://localhost:8082/payment/deposit/',
    withdrawRequest: 'http://localhost:8082/payment/withdraw/',
  }
};