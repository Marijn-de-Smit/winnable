import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { Util } from 'src/app/helpers/util';
import { Restdata } from 'src/app/helpers/restdata';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
  )
}

@Injectable()
export class WalletService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private restData: Restdata) {
  }

  private handleError(error: Response) {
    console.error(error);
    return throwError(Util.createErrorMessage(error));
  }

  getbyId(id) {
    return this.http.get<any>(this.restData.getUrl + '/userservice/get/' + id, this.restData.httpOptions);
  }

  update(dto) {
    return this.http.post(this.restData.getUrl('userservice/wallet/updateBalance'), JSON.stringify(dto), this.restData.httpOptions);
  }
}
