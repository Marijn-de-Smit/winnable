import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { Util } from 'src/app/helpers/util';
import { Restdata } from 'src/app/helpers/restdata';
import { catchError } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
  )
}

@Injectable()
export class LotteryService {

  constructor(
    private http: HttpClient,
    private router: Router,
    private restData: Restdata) {
  }

  private getHeaders(){
    const token = localStorage.getItem('token');
    return {
      headers: new HttpHeaders({
        Authorization: 'Bearer ' + token,
        'Content-Type': 'application/json'
      })};
  }

  private handleError(error: Response) {
    console.error(error);
    return throwError(Util.createErrorMessage(error));
  }

  create(dto) {
    return (this.http.post<any>(this.restData.getUrl('providerservice/lottery/create'), dto, this.getHeaders() )).pipe(catchError(this.handleError));
  }

  getAll() {
    return (this.http.get<any>(this.restData.getUrl('providerservice/lottery/all'), this.getHeaders())).pipe(catchError(this.handleError));;
  }

  getbyId(id) {
    return (this.http.get<any>(this.restData.getUrl('providerservice/lottery/get/' + id), this.getHeaders())).pipe(catchError(this.handleError));
  }

  update(dto) {
    return (this.http.put(this.restData.getUrl('providerservice/lottery/update'), dto, this.getHeaders())).pipe(catchError(this.handleError));
  }

  delete(id) {
    return (this.http.delete(this.restData.getUrl('providerservice/lottery/delete/' + id), this.getHeaders())).pipe(catchError(this.handleError));
  }

  purchase(dto){
    return (this.http.put(this.restData.getUrl('lotteryservice/ticket/update'), this.getHeaders())).pipe(catchError(this.handleError));
  }
}
