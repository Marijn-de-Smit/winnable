import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Router} from '@angular/router';
import {AuthenticationDTO} from 'src/app/helpers/dto/AuthenticationDTO';
import {Observable, throwError} from 'rxjs';
import {Restdata} from "src/app/helpers/restdata";
import { catchError } from 'rxjs/operators';
import { RegistrationRequestDTO } from 'src/app/helpers/dto/RegistrationDTO';
import { Util } from 'src/app/helpers/util';

const httpOptions = {
  headers: new HttpHeaders(
    {
      'Content-type': 'application/json',
      'Access-Control-Allow-Origin': '*'
    }
  )
}

@Injectable()
export class AuthenticationService {

  authenticationDTO: AuthenticationDTO;
  redirectUrl: string;

  constructor(private http: HttpClient, private router: Router, private restData: Restdata) {
  }

  private handleError(error: Response) {
    console.error(error);
    return throwError(Util.createErrorMessage(error));
  }

   //login
   public login(loginRequest: AuthenticationDTO) {
    return (this.http.post<any>(this.restData.getUrl('userservice/authentication/login'), loginRequest,
    )).pipe(catchError(this.handleError));
  }
 
   //register
   public register(registerRequest: RegistrationRequestDTO) {
    return (this.http.post<any>(this.restData.getUrl('userservice/authentication/register'), registerRequest, this.restData.httpOptions
    )).pipe(catchError(this.handleError));
  }

  //registrationConfirm
  public registrationConfirm(token: String) {
    return (this.http.get<any>(this.restData.getUrl('userservice/authentication/registrationConfirm') + token,
    )).pipe(catchError(this.handleError));
  }

  //logout
  public logout(){
    
  }
}
