import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
import { AuthenticationDTO } from 'src/app/helpers/dto/AuthenticationDTO';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { RegistrationRequestDTO } from 'src/app/helpers/dto/RegistrationDTO';
import { Util } from 'src/app/helpers/util';
import { Restdata } from 'src/app/helpers/restdata';
import { DeleteAccountDTO } from 'src/app/helpers/dto/account/DeleteAccountDTO';

const httpOptions = {
    headers: new HttpHeaders(
        {
            'Content-type': 'application/json',
            'Access-Control-Allow-Origin': '*'
        }
    )
}

@Injectable()
export class AccountService {

    constructor(private http: HttpClient, private router: Router, private restData: Restdata) {
    }

    private handleError(error: Response) {
        console.error(error);
        return throwError(Util.createErrorMessage(error));
    }

    public retrieveAllUsers() {
        return (this.http.get<any>(this.restData.getUrl('userservice/account/users'),
        )).pipe(catchError(this.handleError));
    }

    public retrieveAllManagers() {
        return (this.http.get<any>(this.restData.getUrl('userservice/account/managers'),
        )).pipe(catchError(this.handleError));
    }

    public retrieveAllProviders() {
        return (this.http.get<any>(this.restData.getUrl('userservice/account/providers'),
        )).pipe(catchError(this.handleError));
    }

    public addUser(addAccountRequest: RegistrationRequestDTO) {
        return (this.http.post<any>(this.restData.getUrl('userservice/account/addUser'), addAccountRequest,
        )).pipe(catchError(this.handleError));
    }

    public addManager(addAccountRequest: RegistrationRequestDTO) {
        return (this.http.post<any>(this.restData.getUrl('userservice/account/addManager'), addAccountRequest,
        )).pipe(catchError(this.handleError));
    }

    public addProvider(addAccountRequest: RegistrationRequestDTO) {
        return (this.http.post<any>(this.restData.getUrl('userservice/account/addProvider'), addAccountRequest,
        )).pipe(catchError(this.handleError));
    }

    public deleteAccount(deleteAccountRequest: DeleteAccountDTO) {
        return (this.http.post<any>(this.restData.getUrl('userservice/account/delete'), deleteAccountRequest,
        )).pipe(catchError(this.handleError));
    }

}
