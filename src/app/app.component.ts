import { Component, OnInit } from '@angular/core';
import * as $ from 'jquery';
import * as M from 'materialize-css'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  title = 'winnable';
  ngOnInit(): void {
    $(document).ready(function(){
      M.AutoInit();
    });
  }
}
